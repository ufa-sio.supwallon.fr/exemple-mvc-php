<?php
namespace MonProjet;

require_once "vendor/autoload.php";

use MonProjet\kernel\View;

View::setTemplate("home.tpl");
View::display();