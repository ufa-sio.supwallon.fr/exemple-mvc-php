<?php
namespace MonProjet\model\classes;

use MonProjet\kernel\AbstractObject;

class Employe extends AbstractObject
{
    private $id;
    protected function getId() { return $this->id; }
    protected function setId($value) { $this->id = $value; }
    
    private $nom;
    protected function getNom() { return $this->nom; }
    protected function setNom($value) { $this->nom = $value; }

    private $prenom;
    protected function getPrenom() { return $this->prenom; }
    protected function setPrenom($value) { $this->prenom = $value; }
}